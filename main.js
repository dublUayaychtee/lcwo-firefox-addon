// https://stackoverflow.com/a/13836786

function getTextNodesIn(node, includeWhitespaceNodes) {
    var textNodes = [],
        whitespace = /^\s*$/;

    function getTextNodes(node) {
        if (node.nodeType == 3) {
            if (includeWhitespaceNodes || !whitespace.test(node.nodeValue)) {
                textNodes.push(node);
            }
        } else {
            for (var i = 0, len = node.childNodes.length; i < len; ++i) {
                getTextNodes(node.childNodes[i]);
            }
        }
    }

    getTextNodes(node);
    return textNodes;
}

let eElement = document.querySelector(
    "body > table:nth-of-type(3) > tbody > tr > td:nth-of-type(1) > table:nth-of-type(1) td"
);

let loginHeader = document.querySelector(
    "body > table:nth-of-type(3) > tbody > tr > td:nth-of-type(1) > table:nth-of-type(1) > tbody > tr:nth-of-type(1) > th"
);

if (loginHeader.textContent !== "Login") {
    eElement.setAttribute("id", "topMenu");

    let elem = document.createElement("div");
    elem.id = "topMenuFirstRow";
    eElement.insertBefore(elem, eElement.firstChild);
    let textnodes = getTextNodesIn(eElement);
    textnodes.forEach((node) => {
        if (node.parentElement.id == "topMenu") {
            const span = document.createElement("span");
            node.after(span);
            span.appendChild(node);
        }
    });

    for (let i = 0; i < 3; i++) {
        document
            .getElementById("topMenuFirstRow")
            .appendChild(eElement.children.item(1));
    }
} else {
    let languageSelector = document.querySelector(
        "body > table:nth-of-type(3) > tbody > tr > td:nth-of-type(1) > table:nth-of-type(2)"
    );
    languageSelector.style["text-align"] = "center";
}

let menuChildren = eElement.children;

let leftMenu = document.querySelector(
    "body > table:nth-of-type(3) > tbody > tr > td:nth-of-type(1)"
);

let logoLink = document.createElement("a");
logoLink.href = "/";
logoLink.id = "nav-logo";

let logoImg = document.createElement("img");
logoImg.src = "/pics/lcwo.png";

logoLink.appendChild(logoImg);

leftMenu.insertBefore(logoLink, leftMenu.firstChild);
